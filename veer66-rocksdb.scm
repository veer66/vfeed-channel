(define-module (veer66-rocksdb)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (gnu packages databases))

(define-public rocksdb-noje
  (package
   (inherit rocksdb)
   (version "6.26.1-noje")
   (arguments
    (substitute-keyword-arguments (package-arguments rocksdb)
       ((#:configure-flags configure-flags)
        `(append '("-DDISABLE_JEMALLOC=1") ,configure-flags))))
   (inputs
    (modify-inputs (package-inputs rocksdb)
                   (delete "jemalloc")))))
